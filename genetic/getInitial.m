function [POP] = getInitial(dim, masa,cost,masa_max)
  ok=0;
  n=length(masa);
  POP=zeros(dim,n+1);
  for i=1:dim
  
    while ~ok
      initial=unifrnd(0,1,1,n);
      if admisibil(initial,masa,masa_max)
        ok=1;
      end
    end
    POP(i,1:n)=initial;
    POP(i,n+1)=f_obiectiv(initial,cost);
    ok=0;
    
  end
  
end
