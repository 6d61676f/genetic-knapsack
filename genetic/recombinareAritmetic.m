function [c1,c2] = recombinareAritmetic (x1,x2,pm,alfa,cost,masa,masa_max)
  
  c1=x1;
  c2=x2;
  
  
  r=unifrnd(0,1);
  
  if r<pm
  
    m=length(x1);
    n=m-1;
    
    for i=1:n
      
      c1(i)=alfa*x2(i)+(1-alfa)*x1(i);
      c2(i)=alfa*x1(i)+(1-alfa)*x2(i);
      
    end
    
    c1(m)=f_obiectiv(c1(1:n),cost);
    c2(m)=f_obiectiv(c2(1:n),cost);
    
    if ~admisibil(c1(1:n),masa,masa_max)
      c1=x1;
    end
    
    if ~admisibil(c2(1:n),masa,masa_max)
      c2=x2;
    end
    
  
  
  end

end
