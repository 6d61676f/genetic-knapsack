function [ok] = admisibil (x, masa, masa_max)
  ok=1;
  n=length(masa);
  i=1;
  while i<=n && ok
    masa_max-=x(i)*masa(i);
    if masa_max < 0
      ok=0;
    end
    i++;
  end
end
