function [parinti] = selectie_sus (pop)
%pop dim x m , m este val fct obiectiv

[dim , m]=size(pop);
obiectiv=pop(1:dim,m);
total_ob = sum(obiectiv);

obiectiv(1:dim)=obiectiv(1:dim)/total_ob;

obiectiv_cumulat=zeros(1,dim);

for i=1:dim
  obiectiv_cumulat(i)=sum(obiectiv(1:i));
end

r=unifrnd(0,1/dim);
i=1;
k=1;

while k<=dim
  
  while r <= obiectiv_cumulat(i)
    
    parinti(k,1:m)=pop(i,1:m);
    r=r+1/dim;
    k=k+1;
      
  end
  
  i=i+1;
  
end

end
