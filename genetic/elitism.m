function [N] = elitism (pop, popN)
  N=popN;
  [dim,n]=size(pop);
  [max1,i]=max(pop(:,n));
  [max2,j]=max(popN(:,n));
  
  if max1 > max2
    [min1,k]=min(popN(:,n));
    N(k,:)=pop(i,:);
  end

end
