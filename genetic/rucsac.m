function [sol] = rucsac (caleCost, caleMasa,masaMax,pm,alfa,sigma,dim)

if dim % 2 
  dim=dim+1;
end

[masa,cost]=load_files(caleMasa,caleCost);

pop=getInitial (dim, masa,cost,masaMax);
popN=pop;

[~,m]=size(pop);

maxVal = max(pop(:,m));
newVal = maxVal;

while newVal <= maxVal

  parinti=selectie_sus(popN);

  for i=1:2:dim-1
    [popN(i,:),popN(i+1,:)]=recombinareAritmetic (parinti(i,:),parinti(i+1,:),pm,alfa,cost,masa,masaMax);
    [popN(i,:)]=mutatieCreep (popN(i,:),pm,sigma,0,1,cost,masa,masaMax);
    [popN(i+1,:)]=mutatieCreep (popN(i+1,:),pm,sigma,0,1,cost,masa,masaMax); 
  end
  
  popN=elitism(parinti,popN);
  
  popN=sortrows(popN,m);
  
  sol=popN(dim,:);
      
  newVal=sol(m);

 end
  
end
