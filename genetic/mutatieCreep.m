function [xN] = mutatieCreep (x,pm,sigma,a,b,cost,masa,masa_max)

m=length(x);
n=m-1;


xN=x;




  for j=1:n
  
    r = unifrnd(0,1);
    
    if r<pm
      
      R=normrnd(0,sigma);
      
      xN(j)=xN(j)+R;
      
      if xN(j) < a
        xN(j)=a;
      else 
        if xN(j) > b
          xN(j)=b;
        end
      end
    
    end
  
  end
  
   xN(m)=f_obiectiv(xN(1:n),cost);
    
    if ~admisibil(xN(1:n),masa,masa_max)
      xN=x;
    end


end
