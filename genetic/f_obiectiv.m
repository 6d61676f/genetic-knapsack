function [val] = f_obiectiv (x,cost)
  n=length(cost);
  val=0;
  for i=1:n
    val += x(i)*cost(i);
  end
end