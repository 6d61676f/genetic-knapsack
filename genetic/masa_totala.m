function [M] = masa_totala (x, masa)
  n=length(x);
  M=0;
  for i=1:n
    M+=x(i)*masa(i);
  end
end
