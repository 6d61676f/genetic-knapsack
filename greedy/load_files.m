function [obiecte] = load_files (fisier_cost, fisier_masa)
  %pe prima coloana e costul, pe a doua masa
  cost=load(fisier_cost);
  masa=load(fisier_masa);
  obiecte=zeros(length(cost),2);
  obiecte(:,1)=cost;
  obiecte(:,2)=masa;
  obiecte=sortrows(obiecte,2);
  for i=1:length(cost)-1
    for j=i:length(cost)
      if obiecte(i,1)/obiecte(i,2) < obiecte(j,1)/obiecte(j,2)
        temp=obiecte(j,:);
        obiecte(j,:)=obiecte(i,:);
        obiecte(i,:)=temp;
      end
    end
  end
endfunction
