function [solutie] = greedy_rucsac (costuri,masa_max)
  i=1;
  n=length(costuri(:,1));
  val=0;
  solutie=zeros(1,n);
  while masa_max>0 && i<=n
    if costuri(i,2)<=masa_max
      val+=costuri(i,1);
      masa_max-=costuri(i,2);
      solutie(i)=1;
    else
      val+=costuri(i,1)*(masa_max/costuri(i,2));
      solutie(i)=masa_max/costuri(i,2);
      masa_max=0;
    endif
    i++;
  endwhile

endfunction
